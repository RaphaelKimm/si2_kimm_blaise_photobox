import * as photoloader from "./photoloader.js";
import * as gallery from "./gallery.js";
import * as lightbox from "./lightbox.js";

export function start() {
  photoloader.init("https://webetu.iutnc.univ-lorraine.fr");
  gallery.init(0,10);
  lightbox.init();
};
