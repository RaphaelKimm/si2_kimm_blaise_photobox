let url;

export function init(serverUrl) {
  url=serverUrl;
};

export function getObjects(serverUri) {
  return axios.get(url+serverUri,{responseType: 'json',
                                  withCredentials: true})
                    .catch((e) => {
                      if (e.response) {
                          console.log(e.response.status);
                      }
                      else if (e.request) {
                        console.log(e.request);
                      }
                      else {
                        console.log("Erreur : "+e.message);
                      }
                    });
};
