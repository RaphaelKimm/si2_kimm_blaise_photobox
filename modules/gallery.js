import * as photoloader from "./photoloader.js";

let PHPargs;
let currentOffset;
let gallery;
var photosOffset;

export function init(offset=null,size=null) {
  PHPargs="/www/canals5/photobox/photos/";
  if (typeof offset === "undefined" && typeof size !== "undefined") {
    PHPargs+="?size="+size;
  }
  else if (typeof offset !== "undefined" && typeof size === "undefined") {
    PHPargs+="?offset="+offset;
    currentOffset=offset;
  }
  else {
    PHPargs+="?offset="+offset+"&size="+size;
    currentOffset=offset;
  }

  $(document).on("click","nav #load_gallery",loadGallery);
  $(document).on("click","nav #next",nextPage);
  $(document).on("click","nav #previous",prevPage);

}

export function getGallery() {
  return gallery;
}

export function loadGallery(){

  let photosPromise=photoloader.getObjects(PHPargs);
  photosPromise.then((response) => {
    gallery=response;
    insertIntoDOM(response)
  });
}

export function nextPage(){

    let photosPromise=photoloader.getObjects(PHPargs);
    photosPromise.then((response) => {
        PHPargs=response.data.links["next"]["href"];
        photosPromise=photoloader.getObjects(PHPargs);
        photosPromise.then((response) => {
          gallery=response;
          insertIntoDOM(response)
        });
    });
}

export function prevPage(){

    let photosPromise=photoloader.getObjects(PHPargs);
    photosPromise.then((response) => {
        PHPargs=response.data.links["prev"]["href"];
        photosPromise=photoloader.getObjects(PHPargs);
        photosPromise.then((response) => {
          gallery=response;
          insertIntoDOM(response);
        });

    });
}

function insertIntoDOM(response) {
  $(".gallery-container").empty();
  var count=currentOffset;
  var photosOffset=[];

  response.data.photos.forEach((e) => {
    count++;
    let content=`<div class='vignette'>
                <img
                    data-img='https://webetu.iutnc.univ-lorraine.fr`+e.photo.original["href"]+`'
                    data-uri='https://webetu.iutnc.univ-lorraine.fr/www/canals5/photobox/photos/`+e.photo["id"]+`'
                    data-id='`+e.photo["id"]+`'
                    src='https://webetu.iutnc.univ-lorraine.fr`+e.photo.thumbnail["href"]+`'>
                    <div>`+e.photo.titre+`</div>
              </div>`;
    $(".gallery-container").append(content);
  });
}
