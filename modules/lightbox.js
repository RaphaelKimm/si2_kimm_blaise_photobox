import * as gallery from "./gallery.js";
import * as photoloader from "./photoloader.js";

export function init() {
  $(document).on("click",".vignette",showLightbox);
  $(document).on("click",".vignette",(e) => {fillLightbox(e,0)});
  $(document).on("click","#lightbox_close",closeLightbox);
  $(document).on("click","#lightbox_previous",prev);
  $(document).on("click","#lightbox_next",next);
}

function showLightbox() {
  $(".lightbox_container").addClass("lightbox_containerShow");
  $(".lightbox_container").offset({
    top: $(window).scrollTop()
  })
  $("body").addClass("bodyHideOverflow");
}

function fillLightbox(e,mode) {
  let src, name, uri, id;

  if (mode===0) {
    if ($(e.target).attr("data-img")) {
      src=$(e.target).attr("data-img");
      uri=$(e.target).attr("data-uri");
      id=$(e.target).attr("data-id");
      name=$(e.target).next().text();
    }
    else {
      if ($(e.target).find("img").length>0){
        src=$(e.target).find("img").attr("data-img");
        uri=$(e.target).find("img").attr("data-uri");
        id=$(e.target).find("img").attr("data-id");
        name=$(e.target).find("img").next().text();
      }
    }
  }
  else {
    src="https://webetu.iutnc.univ-lorraine.fr/"+e.photo["original"]["href"];
    id=e.photo["id"];
    name=e.photo["titre"];
  }

  let g=gallery.getGallery();

  if (g.data.photos[0].photo["id"]==id) {
    $("#lightbox_previous").addClass("inoperative");
  }
  else {
    $("#lightbox_previous").removeClass("inoperative");
  }

  if (g.data.photos[g.data.photos.length-1].photo["id"]===id) {
    $("#lightbox_next").addClass("inoperative");
  }
  else {
    $("#lightbox_next").removeClass("inoperative");
  }

  $("#lightbox-img").append(`<img id='lightbox_full_img' src='`+src+`'
                             data-uri='`+uri+`'
                             data-id='`+id+`'>`);
  $("#lightbox_title").replaceWith(`<h1 id="lightbox_title">`+name+`</h1>`);
}

function closeLightbox() {
  $("#lightbox-img").empty();
  $("body").removeClass("bodyHideOverflow");
  $(".lightbox_container").removeClass("lightbox_containerShow");
}

function prev() {
  let id=$("#lightbox_full_img").attr("data-id");
  let photo=getPreviousPhoto(id);
  if (photo!=null) {
    $("#lightbox-img").empty();
    fillLightbox(photo,1);
  }
}

function next() {
  let id=$("#lightbox_full_img").attr("data-id");
  let photo=getNextPhoto(id);
  if (photo!=null) {
    $("#lightbox-img").empty();
    fillLightbox(photo,1);
  }
}

function getPreviousPhoto(id) {
  let i;
  let prevImage=null;
  for (i=0;i<gallery.getGallery().data.photos.length;i++) {
    if (gallery.getGallery().data.photos[i].photo["id"]==id) {
      if (i-1>=0)  {
        prevImage=gallery.getGallery().data.photos[i-1];
        return prevImage;
      }
    }
  }
}

function getNextPhoto(id) {
  let i;
  let nextImage=null;
  for (i=0;i<gallery.getGallery().data.photos.length;i++) {
    if (gallery.getGallery().data.photos[i].photo["id"]==id) {
      if (i+1<=gallery.getGallery().data.photos.length-1) {
        let nextImage=gallery.getGallery().data.photos[i+1];
        return nextImage;
      }
    }
  }
  return nextImage;
}
